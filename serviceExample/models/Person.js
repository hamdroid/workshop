/**
 * Created by adhach29 on 27.02.2017.
 */

let mongoose = require('mongoose');
module.exports = mongoose.model("Person", createSchema(mongoose));

function createSchema(mongoose) {
    return mongoose.Schema({
        "name": String,
        "speed": String,
        "age": String
    }, {
        "collection": "heroes"
    });
}