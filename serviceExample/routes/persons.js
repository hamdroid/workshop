/**
 * Created by adhach29 on 27.02.2017.
 */

let express = require('express');
let Person = require('../models/Person');
let router = express.Router();



router.post('/put', (req, res, next) => {
    let myPerson = req.body;

    if (isPerson(myPerson)) {
        new Person(myPerson).save();
        res.sendStatus(200);
        return;
    }

    res.sendStatus(500);
});

router.post('/get', (req, res, next) => {

    Person.find({}, (err, people) => {
        if (err) {
            res.json(err);
            return;
        }
        res.json(people);
    });
});

function isPerson(person) {
    return person.hasOwnProperty("name") &&
        person.hasOwnProperty("speed") &&
        person.hasOwnProperty("age");
}

module.exports = router;
