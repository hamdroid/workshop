import $ from 'jquery';
import _ from 'lodash';
import config from '../config.js';

(function ($, _, config) {
    let main = $("#myDiv");
    let paragraph = $("<span></span>");

    $.post(config.getPersons, {}, function (data) {
        let formatted = _.map(data, (person) => {
            return `Hi, my name is <b>${person.name}</b> and this is my speed: <b>${person.speed}</b>`;
        }).join("</br>");

        paragraph.html(`<span>${formatted}</span>`);
        main.append(paragraph);
    });
}($, _, config));